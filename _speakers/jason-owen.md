---
name: Jason Owen
talks:
- "Python Grab Bag: A Set of Short Plays"
---
[Jason Owen](http://jasonaowen.net/) is a consultant and programmer
([Twitter](https://twitter.com/jasonaowen),
[GitHub](https://github.com/jasonaowen)) who learned a tremendous amount
from participating in [the Computer Action Team at Portland State
University](http://cat.pdx.edu/) and from independent study at [the Recurse
Center](https://www.recurse.com/). He is a technology generalist, polyglot
programmer, sometimes database administrator and sysadmin, and cares deeply
about code review, continuous integration, and crafting good software.

[Sumana](/speakers/sumana-harihareswara/) and Jason presented the play [Code Review, Forwards and
Back](https://2017.pygotham.org/talks/code-review-forwards-and-back/) at
PyGotham 2017.
