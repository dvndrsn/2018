---
name: Hayley Denbraver
talks:
- 'We Are 3000 Years Behind: Let''s Talk About Engineering Ethics'
---

Hayley Denbraver is a web developer in San Diego, California. She is a career switcher who used to work as a licensed civil engineer. Her civil engineering projects included working on a hotel near Disneyland (It is awesome!) and anchoring air conditioners to hospital roofs (They are awesome too!). As a civil engineer she had a stamp with her name on it, but as a web developer you can find her name by using git blame. In her current work she doesn't have a phone on her desk, a fact which her employers should use to recruit new people. Not having a phone on your desk is awesome. She is an avid contributor to her company's #cute-animals slack channel. Her favorite part of her job includes helping to onboard new, internally trained devs. She is a member of a two developer household, which you should consider before accepting her dinner party invitation. Her 2018 stretch goal is to acquire a golden retriever--name suggestions are welcome.