---
title: PyGotham 2018 is sold out!
date: 2018-10-02 00:00:00 -0400
excerpt_separator: <!--more-->
---

PyGotham 2018 is sold out! Thank you to everyone who got a ticket, and we'll see
you all bright and early on Friday, October 5th!

<!--more-->

If you're attending the conference, be sure to check out the
[schedule](/talks/schedule/) and to sign up for the Friday evening [social
event](/events/social-event/).
