---
title: Code of Conduct Transparency Report
date: 2018-11-11 00:00:00 -0500
excerpt_separator: <!--more-->
---

PyGotham strives to be a friendly and welcoming environment, and we take our
code of conduct seriously. In the spirit of transparency, we publish a summary
of the reports we received this year.

<!--more-->

We appreciate those reports, and we encourage anyone who witnesses a code of
conduct violation to report it via the methods listed at
<{{ site.url }}{% link about/code-of-conduct.md %}>. PyGotham 2018 had eight
code of conduct reports made to the organizers. Anonymized details of these
reports are below.

During the public voting phase of talk selection:

- Three talk proposals were reported as potential code of conduct violations
  without specific detail. These were erroneous votes made by the reporters and
  were retracted when PyGotham staff requested details. We’ve noted that our
  report button is not clearly differentiated from the other voting buttons and
  plan to remedy that for the future.

- A talk proposal was reported by multiple voters as containing offensive
  subject matter and potentially advocating harassment. The proposal was removed
  from consideration during the public voting phase. We thank the individuals
  who reported this talk proposal for helping us maintain a welcoming
  environment for all PyGotham attendees.

- A talk proposal used potentially exclusionary language. After clarifying the
  issue with the reporter, the proposal’s author has been asked to update their
  language in the talk description. The talk’s content does not inherently
  violate PyGotham’s code of conduct, and this report did not result in its
  immediate removal from consideration for the conference.

- A talk proposal was reported as potentially containing offensive content.
  PyGotham staff asked the proposal’s author for clarification. The author
  assured us that the talk would not contain any content in violation of the
  code of conduct and that all content included in the talk would be relevant to
  the technical aspects. The talk was not removed from consideration for the
  conference.

At the conference:

- A speaker used the term “non-traditional” to describe same-sex relationships.
  Conference staff contacted the speaker to inform them of the inappropriate
  label. The speaker clarified that this was intended to quote the political
  viewpoint of some countries of the world and not representation of the
  speaker’s views. The speaker plans to rephrase this for clarity in future
  versions of the talk. Staff relayed the speaker’s response to the reporter,
  who accepted that this was a simple misunderstanding.

- An attendee joined a group of people having a conversation about treatment of
  people in the workplace. The attendee said the conversation was too heavy for
  breakfast and that the others should be more “positive,” making them feel
  uncomfortable. Staff spoke to the attendee who apologized and expressed
  remorse, acknowledging that they should have moved on rather than trying to
  change the conversation. Staff relayed the attendee’s response to the
  reporter.
